#------------------------------------------------------------------------------#
from hashlib import new
from typing import ItemsView
from pymongo import MongoClient
import time
from bson.son import SON
import datetime
import matplotlib.pyplot as plt
import numpy as np
#------------------------------------------------------------------------------#

#--------------------#
# Author : Paul Briand
# Date : 08/09/2021
#--------------------#

'''
    current database structure:
    {
        '_id': ObjectId('...'), 
        'Aimée Boyer': {
            'city': 'Gay', 
            'job': 'expert-comptable', 
            'iban': 'FR6892192057248937929043616', 
            'color': '#68fffc', 
            'phone': '0365275246', 
            'birthdate': '2000-09-04', 
            'music': ['rock', 'trance', 'metal', 'eurodance']
        }
    }
'''
def get_database():

    CONNECTION_STRING = "mongodb://rhobs:xeiPhie3Ip8IefooLeed0Up6@15.236.51.148/?authSource=rhobs"
    # Create a connection using MongoClient. 
    client = MongoClient(CONNECTION_STRING)
    return client['rhobs']

def concatenate_dict(dic1, dic2):

    return dict(list(dic1.items()) + list(dic2.items()))

def get_age(birthdate):

    today = datetime.date.today()
    born = datetime.datetime.strptime(birthdate,'%Y-%m-%d').date()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

def print_aggregation(agg):
    print("--------------------------------------------------")
    for item in agg:
        print(item)
    print("--------------------------------------------------")

def flatten_collection(collection):
    '''
        we wish to have this new structure:
        {
            '_id': ObjectId('...'), 
            'name' : 'Aimée Boyer'
            'city': 'Gay', 
            'job': 'expert-comptable', 
            'iban': 'FR6892192057248937929043616', 
            'color': '#68fffc', 
            'phone': '0365275246', 
            'birthdate': '2000-09-04', 
            'music': ['rock', 'trance', 'metal', 'eurodance'], 
            'age': 21
        }
    '''

    myclient = MongoClient("mongodb://localhost:27017/")
    print("List of databases before deletion\n--------------------------")
    for x in myclient.list_database_names():
        print(x)
    #delete database named 'mydatabase'
    myclient.drop_database('mydatabase')
    print('--------------------------\nmydatabase deleted.')
    time.sleep(2)
    print("Starting flattening collection.")
    mydb = myclient["mydatabase"]
    mycol = mydb["flat"]
    for item in collection.find():
        flat = list(item.items())
        name = flat[1][0]
        inserter = dict([flat[0]])
        inserter = concatenate_dict(inserter,{'name': name})
        inserter = concatenate_dict(inserter, flat[1][1])
        inserter = concatenate_dict(inserter, {'age': get_age(inserter['birthdate'])})
        mycol.insert_one(inserter)
    print("Flattening ended.")
    time.sleep(3)
    return mycol

#0# THE NUMBER OF LISTENER BY MUSIC #0#

def listener_by_music(collection):
        pipeline = [
            {"$unwind": "$music"},
            {"$group": {"_id": "$music", "nb_listeners": {"$sum": 1}}},
            {"$sort": SON([("nb_listeners", -1), ("_id", -1)])}
        ]
        agg = collection.aggregate(pipeline)
        return agg

#1# THE AVERAGE AGE BY MUSIC #1#

def age_by_music(collection):
        pipeline = [
            {"$unwind": "$music"},
            {"$group": {"_id": "$music", "avg_age":  {"$avg": "$age"}}},
            {"$sort": SON([("avg_age", -1), ("_id", -1)])},
        ]
        agg = collection.aggregate(pipeline)
        return agg

#2# THE PYRAMID AGE #2#

def pyramid_age(collection, city, slice_size):
        population = collection.count_documents({"city" : city})
        print(population)
        pipeline = [
            {"$match" : {"city" : city}},
            {"$addFields" : {"age_slice" : {"$toInt": {"$floor" : {"$divide" : ["$age", slice_size]}}}}},
            {"$group" : {"_id" : "$age_slice", "count" : {"$sum" : 1}}},
            {"$addFields" : {"percentage" : {"$multiply" : [{"$divide" : ["$count", population]}, 100]}}},
            {"$sort": SON([("_id", 1)])},
            {"$unset" : "count"}
        ]
        agg = collection.aggregate(pipeline)
        return agg


if __name__ == "__main__":    
    
    # Get the database and the collection

    dbname = get_database()
    rhobs_collection = dbname["test"]
    #flattening the collection to get values inside the name object
    flat_collection = flatten_collection(rhobs_collection)

    #0# THE NUMBER OF LISTENER BY MUSIC #0#

    print_aggregation(listener_by_music(flat_collection))

    #1# THE AVERAGE AGE BY MUSIC #1#

    print_aggregation(age_by_music(flat_collection))

    #2# THE PYRAMID AGE #2#

    city = "Gay"
    slice_size = 10
    agg = pyramid_age(flat_collection, city, slice_size)
    aux = []

    for item in agg:
        aux.append(item)
    print_aggregation(aux)

    # PLOTTING THE PYRAMID #

    height = [item['percentage'] for item in aux]
    bars = tuple(["{min}-{max}".format(min = item['_id']*slice_size, max = item['_id']*(slice_size + 1) - 1) for item in aux])
    y_pos = np.arange(len(bars))
    plt.barh(y_pos, height)
    plt.yticks(y_pos, bars)
    plt.title("Pyramid age in the city of {city} with a slice size of {size}".format(city = city, size = slice_size))
    plt.xlabel("Percentage")
    plt.ylabel("Age")
    plt.show()